import math
import os

import pytest
from task.engine import Engine
from task.engine import generate_file


@pytest.mark.file_generator
def test_file_generator(create_tempfile):
    tempfile_path = create_tempfile()

    lines_qty = 100
    max_length = 100
    generate_file(tempfile_path, lines_qty, max_length)

    with open(tempfile_path, 'r') as f:
        actual_lines_qty = 0
        for line in f:
            actual_lines_qty += 1

            assert 0 <= len(line.rstrip('\n')) <= max_length

    assert lines_qty == actual_lines_qty


@pytest.mark.sorting
def test_chunk_files_qty_for_small_file(input_path, output_path):
    k = 5
    memory_size = 10
    lines_qty = memory_size * k + 1

    generate_file(input_path, lines_qty)
    engine = Engine(input_path, output_path, memory_size)

    engine.run()

    # При мёрже файлов элементы из всех файлов поместятся в память,
    # поэтому не потребуется использовать жёсткий диск.
    assert len(engine.get_chunk_files()) == k + 1


@pytest.mark.sorting
def test_chunk_files_qty_for_big_file(input_path, output_path):
    k = 20
    memory_size = 10
    lines_qty = memory_size * k + 1

    generate_file(input_path, lines_qty)
    engine = Engine(input_path, output_path, memory_size)

    engine.run()
    # На начальном этапе получится 21 вспомогательный файл.
    # Для мёржа придётся использовать 3 дополнительных файла (10 + 10 + 1)
    assert len(engine.get_chunk_files()) == k + 1 + math.ceil(1. * (k + 1) / memory_size)


@pytest.mark.sorting
def test_chunk_files_are_droped(input_path, output_path):
    k = 20
    memory_size = 10
    lines_qty = memory_size * k + 1

    generate_file(input_path, lines_qty)
    engine = Engine(input_path, output_path, memory_size)

    engine.run()
    for chunk_file in engine.get_chunk_files():
        assert not os.path.exists(chunk_file.name)


@pytest.mark.sorting
def test_output_is_sorted(input_path, output_path):
    memory_size = 20
    lines_qty = 100

    generate_file(input_path, lines_qty)
    engine = Engine(input_path, output_path, memory_size)

    engine.run()

    with open(output_path, 'r') as f:
        prev_line = None
        for line in f:
            if prev_line is not None and line:
                assert line >= prev_line

            prev_line = line

    with open(output_path, 'r') as f:
        output_lines = f.read().split('\n')

    assert len(output_lines) == lines_qty

    with open(input_path, 'r') as f:
        input_lines = f.read().split('\n')

    assert set(input_lines) == set(output_lines)
