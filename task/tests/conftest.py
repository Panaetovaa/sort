import os
import tempfile

import pytest


@pytest.fixture(scope='session')
def tempdir_path():
    path = tempfile.mkdtemp()

    yield path

    os.rmdir(path)


@pytest.fixture
def create_tempfile(tempdir_path):
    _created = []

    def generate():
        f = tempfile.NamedTemporaryFile(dir=tempdir_path, delete=False)
        f.close()
        _created.append(f)

        return f.name

    yield generate

    for f in _created:
        os.remove(f.name)


@pytest.fixture
def input_path(create_tempfile):
    yield create_tempfile()


@pytest.fixture
def output_path(create_tempfile):
    yield create_tempfile()
