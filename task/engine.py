import logging
import os
import random
import string
import time

from typing import Iterable
from typing import List

import tempfile
import heapq


logger = logging.getLogger(__name__)


class Engine:
    def __init__(
            self, input_file_path: str, output_file_path: str,
            memory_size: int = 100, temp_dir: str = '/tmp',
            drop_temp_files=True,
    ):
        self.input_file_path = input_file_path
        self.output_file_path = output_file_path
        self.memory_size = memory_size

        self._auto_drop_temp_files = drop_temp_files
        self._chunk_files = []
        self._temp_dir = self._create_temp_dir(temp_dir)

    def run(self):
        self._create_chunk_files()
        with open(self.output_file_path, 'w') as output_file:
            write_lines(output_file, self._merge_chunk_files())

        if self._auto_drop_temp_files:
            self.drop_temp_files()

    def drop_temp_files(self):
        self._drop_chunk_files()
        self._drop_temp_dir()

    def get_chunk_files(self):
        return self._chunk_files

    def _flush_chunk_file(self, lines: Iterable):
        chunk_file = tempfile.NamedTemporaryFile(dir=self._temp_dir, mode='w+')
        write_lines(chunk_file, lines)
        chunk_file.seek(0)
        self._chunk_files.append(chunk_file)
        return chunk_file

    def _create_chunk_files(self):
        buffer = []
        size = 0

        with open(self.input_file_path, 'r') as input_file:
            for line in input_file:
                buffer.append(line.rstrip('\n'))
                size += 1

                if size >= self.memory_size:
                    self._flush_chunk_file(sorted(buffer))
                    buffer = []
                    size = 0

        if buffer:
            self._flush_chunk_file(sorted(buffer))

    def _drop_chunk_files(self):
        for f in self._chunk_files:
            f.close()

    def _drop_temp_dir(self):
        os.rmdir(self._temp_dir)

    def _merge_chunk_files(self):
        files = self._chunk_files

        while True:
            lines_streams = self._merge([(line.rstrip('\n') for line in f) for f in files])
            if len(lines_streams) == 1:
                return lines_streams[0]

            files = [self._flush_chunk_file(lines_stream) for lines_stream in lines_streams]

    def _create_temp_dir(self, root):
        temp_dir = os.path.join(root, str(time.time()))
        os.mkdir(temp_dir)
        return temp_dir

    def _merge(self, iterables: List[Iterable]):
        chunks = [iterables[i:i + self.memory_size] for i in range(0, len(iterables), self.memory_size)]
        return [heapq.merge(*chunk) for chunk in chunks]


def generate_file(path, lines_qty=100, lines_max_length=100):
    with open(path, 'w') as f:
        lines = (get_random_string(lines_max_length) for _ in range(lines_qty))
        write_lines(f, lines)


def get_random_string(max_length: int):
    letters = string.ascii_letters
    length = random.randint(1, max_length)
    return ''.join(random.choice(letters) for i in range(length))


def write_lines(f, lines):
    is_first_line = True
    for line in lines:
        if is_first_line:
            is_first_line = False
        else:
            f.write('\n')

        f.write(line)
