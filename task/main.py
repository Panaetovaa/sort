"""
Напишите построчную сортировку большого текстового файла, не влезающего в память.
Размер требуемой памяти не должен зависеть от размера файла.
Для проверки работоспособности, нужен генератор таких файлов,
принимающий в качестве параметров количество строк и их максимальную длину.
"""

import click
import logging
from task.engine import Engine
from task.engine import generate_file as call_generate_file


logger = logging.getLogger(__name__)


@click.group()
def cli():
    pass


@cli.command()
@click.option('--lines', type=int, default=100)
@click.option('--length', type=int, default=100)
@click.argument('file_path', required=True)
def generate_file(lines, length, file_path):
    call_generate_file(file_path, lines, length)


@cli.command()
@click.option('--out', 'output_file_path', default='out.txt', required=True)
@click.option('--memory_size', type=int, default=100)
@click.argument('input_file_path', required=True)
def sort_file(input_file_path, output_file_path, memory_size):
    try:
        Engine(input_file_path, output_file_path, memory_size=memory_size).run()
    except:
        logger.exception("Operation failed")


if __name__ == '__main__':
    cli()
