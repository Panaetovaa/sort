FROM python:3.7-alpine

ARG NEXUS_PYPI_INDEX_URL

ENV HOME="/dude"
ENV PYTHONPATH=$HOME/task

RUN addgroup --gid 654 dudes && \
    adduser --uid 543 -g dudes --disabled-password -s /bin/sh dude && \
    mkdir -p ${HOME}

WORKDIR ${HOME}/task

COPY --chown=dude:dudes . ${HOME}/task

RUN pip install -i "${NEXUS_PYPI_INDEX_URL:-https://pypi.org/simple}" -r "$HOME/task/requirements.txt"

USER dude
ENTRYPOINT ["/dude/task/docker-entrypoint.sh"]
