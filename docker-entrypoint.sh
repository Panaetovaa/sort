#!/usr/bin/env sh

case "$1" in
    "sh")
        echo "Starting sh ..."
        exec /bin/sh
        ;;

    *)
        exec python /dude/task/task/main.py "$@"
        ;;
esac
