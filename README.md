# File sorting

## Usage


### Create sample file
Create file with 200 lines and maximum length of lines = 100

    docker run -v /tmp:/tmp testantispam/sortfile:0.2.0 generate-file --lines 200 --length 100 /tmp/out.txt

### Sort file

   Sort file /tmp/in.txt with memory for 10 lines

    docker pull testantispam/sortfile:0.2.0
    docker run -v /tmp:/tmp testantispam/sortfile:0.2.0 sort-file --memory_size 10 --out /tmp/out.txt /tmp/in.txt 


